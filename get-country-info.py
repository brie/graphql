import http.client

conn = http.client.HTTPSConnection("countries.trevorblades.com")

payload = "{\"query\":\"query {\\n  countries {\\n    name\\n    capital\\n       emoji\\n    emojiU\\n      }\\n}\"}"

headers = {
    'Content-Type': "application/json",
    'Authorization': "Bearer "
    }

conn.request("POST", "/", payload, headers)

res = conn.getresponse()
data = res.read()

print(data.decode("utf-8"))

country_data = data.decode("utf-8")

with open('country_data.json', 'w') as f:
    f.write(country_data)
