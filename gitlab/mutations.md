# Mutations

> Changing things. 


Toggling the value of the [keep latest artifact](https://gitlab.com/gitlab-org/gitlab/-/issues/241026) setting for a project:

```
mutation{
  projectCiCdSettingsUpdate(input: {fullPath: "brie/pipeline-cleaner", keepLatestArtifact: true}) {
    clientMutationId
    errors
    ciCdSettings {
      jobTokenScopeEnabled
      keepLatestArtifact
      mergePipelinesEnabled
      mergeTrainsEnabled
    }
  }
}
```



I _believe_ this is presently only possible via GraphQL (not REST), for the project-level setting.
