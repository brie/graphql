# Get raw file content and metadata via GraphQL


[adapted from this](https://stackoverflow.com/a/71353036) SO answer

```
query {
  project(fullPath: "brie/infra") {
    repository {
      blobs(ref:"main", paths: ["README.md", ".gitlab-ci.yml"]) {
        nodes {
          rawBlob
          historyPath
          rawSize
          
        }
      }
    }
  }
}
```