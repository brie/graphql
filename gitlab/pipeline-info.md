# Pipeline Info


:cactus:

From [this blog post](https://www.volkerkrause.eu/2022/07/09/kde-gitlab-ci-api-access.html):


```graphql
query {
  group(fullPath:"games") {
    projects {
      nodes {
        name
        fullPath
        pipelines(ref: "release/22.04", first: 1) {
          nodes { status }
        }
      }
    }
  }
}
```


```
query {
  group(fullPath:"gitlab-gold/briecarranza") {
    projects {
      nodes {
        name
        fullPath
        pipelines(ref: "main", first: 2) {
          nodes { status id detailedStatus {
            id
            text
          } }
        }
      }
    }
  }
}
```


See [this project](https://invent.kde.org/vkrause/kde-ci-status) for more examples from the folks at KDE. 




