# Graph Traversal

> This is where things get really fun!

```
query {
  project(fullPath: "gitlab-org/graphql-sandbox") {
    name
    issues {
      nodes {
        title
        description
      }
    }
  }
}
```

:cactus: Building from the above:

Just get the first two issues:

```
query {
  project(fullPath: "gitlab-org/graphql-sandbox") {
    name
    issues (first: 2) {
      nodes {
        title
        description
      }
    }
  }
}
```

Get the first two issues and for the issue authors, get some info (id, name, public email)


```
query {
  project(fullPath: "gitlab-org/graphql-sandbox") {
    name
    issues (first: 2) {
      nodes {
        title
        description
        webUrl
        author
        {
          id
          name
          publicEmail
        }
      }
    }
  }
}
```

---


```
query {
  project(fullPath: "brie/infra") {
    name
    issues (first: 2) {
      nodes {
        title
        description
        webUrl
        assignees {
          edges {
            node {
              id
              name
              publicEmail
              avatarUrl
              status {
                emoji
                message
              }
            }
          }
        }
        author
        {
          id
          name
          publicEmail
        }
      }
    }
  }
}
```


```
query {
  currentUser {
    id
    name    # Get the name of the current user
    snippets {       # Get snippets owned by the current user
      edges {        # Specifically, this info about the snippets
        node {
          id
          rawUrl
          blobs {
            edges {
              node {
                externalStorage
                mode
                name
                path
                # plainData
                rawPlainData
                # richData
              }
            }
          }
          descriptionHtml
          fileName
        }
      }
    }
    status {
      emoji
      
    }	
  }
}
```

