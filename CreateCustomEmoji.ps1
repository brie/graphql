$headers=@{}
$headers.Add("Content-Type", "application/json")
$headers.Add("Authorization", "Bearer $PAT_FOR_ADDING_CUSTOM_EMOJI")
$response = Invoke-WebRequest -Uri 'https://gitlab.com/api/graphql' -UseBasicParsing -Method POST -Headers $headers -ContentType 'application/json' -Body '{"query":"mutation {\n  createCustomEmoji(input: { groupPath: \"brie-carranza-archives\", name: \"meowheart00\", \n    url: \"https://slackmojis.com/emojis/9845-meow_heart/image/1643514958/meow_heart.png\"}) {\n    clientMutationId\n    errors\n    customEmoji {\n      name\n    }\n  }\n}\n"}'
