import http.client

conn = http.client.HTTPSConnection("gitlab.example.com")

payload = "{\"query\": \"query { queryComplexity {   limit    } }\"}"

headers = {
    'Content-Type': "application/json",
    'Authorization': "Bearer glpat-"
    }

conn.request("POST", "/api/graphql", payload, headers)

res = conn.getresponse()
data = res.read()

print(data.decode("utf-8"))
