require 'uri'
require 'net/http'
require 'openssl'

url = URI("https://gitlab.com/api/graphql")

http = Net::HTTP.new(url.host, url.port)
http.use_ssl = true
http.verify_mode = OpenSSL::SSL::VERIFY_NONE

request = Net::HTTP::Post.new(url)
request["Content-Type"] = 'application/json'
request["Authorization"] = 'Bearer $CI_JOB_TOKEN'
request.body = "{\"query\":\"mutation {\\n  createCustomEmoji(input: { groupPath: \\\"brie-carranza-archives\\\", name: \\\"$PAT_FOR_ADDING_CUSTOM_EMOJI\\\", \\n    url: \\\"https://slackmojis.com/emojis/9845-meow_heart/image/1643514958/meow_heart.png\\\"}) {\\n    clientMutationId\\n    errors\\n    customEmoji {\\n      name\\n    }\\n  }\\n}\\n\"}"

response = http.request(request)
puts response.read_body
