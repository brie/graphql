import os
import sentry_sdk

sentry_dsn = os.environ.get('SENTRY_DSN')


sentry_sdk.init(
    dsn=sentry_dsn,

    # Set traces_sample_rate to 1.0 to capture 100%
    # of transactions for performance monitoring.
    # We recommend adjusting this value in production.
    traces_sample_rate=1.0
)

# sentry_dsn = os.environ.get('SENTRY_DSN')
# print(sentry_dsn)


# division_by_zero = 1 / 0

# Python code to illustrate
# working of try() 
def divide(x, y):
    try:
        # Floor Division : Gives only Fractional
        # Part as Answer
        result = x // y
        print("Yeah ! Your answer is :", result)
    except ZeroDivisionError:
        print("Sorry ! You are dividing by zero ")
   
# Look at parameters and note the working of Program
divide(3, 2)
divide(3, 0)





try:
    age = str(sixteen)
    print("I see that you are %d years old." % age)
except ValueError:
    print("Hey, that wasn't a number!")












