# GraphQL 

## :purple_heart: :fox:  GitLab :fox: :purple_heart: 

  - [GraphiQL](https://gitlab.com/-/graphql-explorer)

### Interesting Uses of the GitLab GraphQL API

  - [KDE CI Status](https://invent.kde.org/vkrause/kde-ci-status)

## :globe_with_meridians:  Explorers :globe_with_meridians: 

> A list of GraphiQL instances, GraphQL Playgrounds and GraphQL APIs.

  - [GraphiQL w/ configurable endpoint!](https://lucasconstantino.github.io/graphiql-online/)
  - [gitlab.com/-/graphql-explorer](https://gitlab.com/-/graphql-explorer)
  - [Pokemon](https://graphqlpokemon.favware.tech/) | https://github.com/favware/graphql-pokemon
  - http://graphql.org/swapi-graphql
  - https://www.predic8.de/fruit-shop-graphql
  - [GraphQL APIs](https://github.com/IvanGoncharov/graphql-apis)
  - [Sourcegraph](https://sourcegraph.com/api/console)
  - [Memair](https://memair.com) - a quantified-self app that uses GraphQL and publishes queries
  - [SpaceX](https://api.spacex.land/graphql/)
  - [Stanford HIV Drug Database](https://hivdb.stanford.edu/page/graphiql/)

**SWAPI** is the **St**ar **W**ars **API**. 

> The Star Wars API, or "swapi" (Swah-pee) is the world's first quantified and programmatically-accessible data source for all the data from the Star Wars canon universe!

Learn more at [swapi.dev](https://swapi.dev/). 

## :bookmark: Bookmarks :bookmark:

  - [Track the `awesome-graphql` list](https://www.trackawesomelist.com/chentsulin/awesome-graphql/week/)
  - [Awesome GraphQL Talks](https://curatedtalks.com/awesome-graphql-talks)
  - [Examples from OTA](https://github.com/basdijkstra/ota-examples/tree/master/python-requests-graphql) blog --> [Writing tests for GraphQL APIs in Python using requests](https://www.ontestautomation.com/writing-tests-for-graphql-apis-in-python-using-requests/)

### Example Queries

  - [Sourcegraph](https://docs.sourcegraph.com/api/graphql/examples)

### :tools: Tools :tools:

  - [GraphQL Code Generator](https://www.graphql-code-generator.com/)
  - [gqt](https://github.com/eerimoq/gqt)
  - [Hasura](https://hasura.io/)



## Queries

[:cat: Take a look at a query with variables. :cactus:](query-with-variables.md)


```
{project(fullPath: "brie/pipeline-cleaner") {
  id
  ciConfigPathOrDefault
}
```

Print the names of each stage in the pipeline with the `iid` **40** in the project `brie/pipeline-cleaner`:

```
query getStageJobs {
  project(fullPath: "brie/pipeline-cleaner") {
    pipeline(iid: "40") {
      stages {
        edges {
          node {
            name
          }
        }
      }
    }
  }
}
```

A working query that uses "groups":

```
query getCiConfigData {
  ciConfig(projectPath: "brie/pipeline-cleaner", content: "include:\n  - template: Security/SAST.gitlab-ci.yml\n\nhi:\n  script:\n    - date\n\n  ") {
    errors
    status
    stages {
      nodes {
        name
        groups {
          nodes {
            name
            jobs { # "__typename": "CiConfigJob"
              nodes {
                name
              }
            }
          }
        }
      }
    }
  }
}
```

Use `groups` and print the `groupName` for each job:

```
query getCiConfigData {
  ciConfig(
    projectPath: "brie/pipeline-cleaner"
    content: "include:\n  - template: Security/SAST.gitlab-ci.yml\n\nhi:\n  script:\n    - date\n\n  "
  ) {
    errors
    status
    stages {
      nodes {
        name
        groups {
          nodes {
            name
            jobs {
              nodes {
                name
                groupName
              }
            }
          }
        }
      }
    }
  }
}
```

A query like the above but we are using the `resource_group` keyword in `.gitlab-ci.yml`:

```
query getCiConfigData {
  ciConfig(
    projectPath: "brie/pipeline-cleaner"
    content: "hi:\n  script:\n    - date\n  resource_group: cat\n\nbuild:\n  stage: build\n  script: \n    - date\n\ndeploy this:\n  stage: deploy\n  script: \n    - date\n  resource_group: production\n\ndeploy that:\n  stage: deploy\n  script: \n    - date\n  resource_group: cat \n"
  ) {
    errors
    status
    stages {
      nodes {
        name
        groups {
          __typename
          nodes {
            name
            jobs {
              nodes {
                name
                groupName
              }
            }
          }
        }
      }
    }
  }
}
```


  - [x] Update this to reference something that does use `resource_groups`


![](groups-of-jobs-for-the-stage.png)


That tells us what the groups of jobs are for: the stage. 

![](ciconfiggroupconnections.png)


The query that generated the screenshot above:

```
query getCiConfigData {
  ciConfig(
    projectPath: "brie/pipeline-cleaner"
    content: "hi:\n  script:\n    - date\n  resource_group: cat\n\nbuild:\n  stage: build\n  script: \n    - date\n\ndeploy this:\n  stage: deploy\n  script: \n    - date\n  resource_group: production\n\ndeploy that:\n  stage: deploy\n  script: \n    - date\n  resource_group: cat \n"
  ) {
    status
    stages {
      nodes {
        name
        groups {
          __typename
          nodes {
            name
            jobs {
              nodes {
                name
                groupName
              }
            }
          }
        }
      }
    }
  }
}
```

  - [GraphQL Lightning Talk](https://gitlab.com/jimsy/graphql-lightning-talk)
  - [Keycloak GraphQL](https://gitlab.com/c2-games/infrastructure/keycloak-graphql)
  - https://gitlab.com/tarantool/third_party/graphql-lua
  - https://gitlab.com/poffey21/graphql
  - https://gitlab.com/contribute2020ctf/challenges/graphql


  - https://gitlab.com/nunet/misc-experiments/issue-analysis-graphql
  - https://gitlab.com/infor-cloud/martian-cloud/tharsis/graphql-query-complexity
  - https://gitlab.com/gitlab-org/govern/compliance/graphql-example-requests



#### :scissors: 

I borrowed from the **Open** [CI/CD Editor "Configuration validation currently not available" error when using `include:file` to include file from another project](https://gitlab.com/gitlab-org/gitlab/-/issues/362315) issue to build out this query. 

  - [Pipeline](https://gitlab.com/api/v4/projects/37398673/pipelines/585877922) | SHA: `4ab1cb52ae34d01a2474592efd4b91215f53b36b`

Replace newlines with `\n` in a file called `file`:

```
# awk '{printf "%s\\n", $0}' file
include:\n  - template: Security/SAST.gitlab-ci.yml\n\nhi:\n  script:\n    - date\n\n
```


## Issues and Milestones


```
query {
  group(fullPath: "brie-carranza-archives") {
    name
    issues(sort: CREATED_ASC) {
      edges {
        node {
          id
          title
          labels {
            edges {
              node {
                title
              }
            }
          }
          milestone {
            id
          }
        }
      }
    }
    }
  }

```

I used the two queries below to create the one immediately above.




```
query {
  group(fullPath: "company") {
    name
    issues(labelName: ["Ordered", "Frontend"], sort: MILESTONE_DUE_ASC) {
        nodes {
            title
            state
            dueDate
            milestone {
              title
            }
        }
    }
  }
```


```
query {
  group(fullPath: "brie-carranza-archives") {
    name
    issues {
      edges {
        node {
          id
          title
          labels {
            edges {
              node {
                id
              }
            }
          }
          milestone {
            id
          }
        }
      }
    }
    }
  }

```

---

# Custom Emoji

## Create custom emoji


See [this issue](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/84304). 

```
mutation {
  createCustomEmoji(input: { groupPath: "brie-carranza-archives", name: "cutecat", url: "https://gitlab.com/brie/graphql/-/raw/main/meow_wow.png"}) {
    clientMutationId
    errors
    customEmoji {
      name
    }
  }
}
```

Use `cURL` to do the above:

```
curl --request POST \
  --url https://gitlab.com/api/graphql \
  --header 'Authorization: Bearer glpat-abcdefghijklmnopqrst' \
  --header 'Content-Type: application/json' \
  --data '{"query":"mutation {\n  createCustomEmoji(input: { groupPath: \"brie-carranza-archives\", name: \"meowheart00\", \n    url: \"https://slackmojis.com/emojis/9845-meow_heart/image/1643514958/meow_heart.png\"}) {\n    clientMutationId\n    errors\n    customEmoji {\n      name\n    }\n  }\n}\n"}'
```        

## Query custom emoji

Get info about custom emoji in all groups for currently authenticated user:

```
query getGroups {
  currentUser {
    name
    groupMemberships {
      edges {
        node {
          # id
           group {
            # id
            name
            webUrl
            customEmoji{
              edges{
                node{
                  name
                  url
                }
              }
            }
          }
        }
      }
    }
  }
}
```



---

# Example Queries

  - https://gitlab.com/gitlab-com/cs-tools/gitlab-cs-tools/project-storage-report/-/blob/main/create_storage_report.py
  - https://wahlnetwork.com/2020/08/04/how-to-query-graphql-apis-with-python/
  - https://www.volkerkrause.eu/2022/07/09/kde-gitlab-ci-api-access.html



# Choosing GraphQL

  - https://nordicapis.com/6-examples-of-graphql-in-production-at-large-companies/
  - 
