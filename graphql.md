Hello, world!

This file was created (probably) via a GraphQL mutation issued in GraphiQL.


The query used to initially create this file looked a lot like this:

```
mutation {
 commitCreate(input: {
    projectPath: "brie/graphql", 
    branch: "main", 
    message: "Created via GraphiQL", 
    actions: [
      {action: CREATE, filePath: "graphql.md", content:"Hello, world!\n\nThis file was created (probably) via a GraphQL mutation issued in GraphiQL."}
    ]
  }) {
    commit {
      title
      authorName
      webPath
      message
    }
    errors
  } 
}


```
