

```
mutation {
  timelogCreate(input: {
    issuableId: "<id of the issue or the MR>",
    timeSpent: "1h 5m",
    spentAt: "2022-07-07",
    summary: "Test from GQL"
  })
  {
    errors
    timelog {
      id
      timeSpent
      spentAt
    }
  }
}

```


