# GraphQL: Queries with Variables

## :school_satchel: About :pencil:

Read the [documentation on GraphQL variables](https://graphql.org/learn/queries/#variables).

## An Example

### Query


```graphql
query($firstN: Int){
  
  allPlanets(first: $firstN) {
    edges {
      node {
        id
        name
        gravity
        terrains
        orbitalPeriod
      }
    }
  }
  
}

```

### Query Variables

```graphql
{"firstN":4}
```
