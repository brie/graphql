
/**
* JetBrains Space Automation
* This Kotlin-script file lets you automate build activities
* For more info, see https://www.jetbrains.com/help/space/automation.html
*/

job("Hello World!") {
    container(displayName = "Say Hello", image = "hello-world")
}
job("Example .sh file") {
    container(displayName = "Run default cmd", image = "alpine") {
        args("echo", "Hello World!")
    }

    container(displayName = "Override default cmd", image = "gradle") {
        mountDir = "/mnt/mySpace"
        workDir = "/mnt/mySpace/work"
        user = "root"
        //override default ENTRYPOINT
        entrypoint("/bin/sh")
        //now, args provide arguments to /bin/sh
        args("-c", "echo \"Project content\" && ls ./ ")
    }
}


job("Build and push Docker") {
    docker {
        build {
            context = "docker"
            file = "./docker/Dockerfile"
            labels["vendor"] = "mycompany"
            args["HTTP_PROXY"] = "http://10.20.30.1:123"
        }

        push("mycompany.registry.jetbrains.space/p/mp/mydocker/myimage") {
            tags("version1.0")
        }
    }
}
