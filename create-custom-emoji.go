package main

import (
	"fmt"
	"strings"
	"net/http"
	"io/ioutil"
)

func main() {

	url := "https://gitlab.com/api/graphql"

	payload := strings.NewReader("{\"query\":\"mutation {\\n  createCustomEmoji(input: { groupPath: \\\"brie-carranza-archives\\\", name: \\\"meowheart00\\\", \\n    url: \\\"https://slackmojis.com/emojis/9845-meow_heart/image/1643514958/meow_heart.png\\\"}) {\\n    clientMutationId\\n    errors\\n    customEmoji {\\n      name\\n    }\\n  }\\n}\\n\"}")

	req, _ := http.NewRequest("POST", url, payload)

	req.Header.Add("Content-Type", "application/json")
	req.Header.Add("Authorization", "Bearer $PAT_FOR_ADDING_CUSTOM_EMOJI")

	res, _ := http.DefaultClient.Do(req)

	defer res.Body.Close()
	body, _ := ioutil.ReadAll(res.Body)

	fmt.Println(res)
	fmt.Println(string(body))

}
